package de.vhv.cucumber;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using="identifierId")
	public WebElement username;
	
	@FindBy(how = How.XPATH, using="//span[text() = 'Weiter']")
	public WebElement submitBtn;
	
	@FindBy(how = How.NAME, using="password")
	public WebElement pw;
	
	@FindBy(how = How.XPATH, using = "//div[text() = 'Schreiben']")
    public WebElement forTheAssertion;	
	
	@FindBy(how = How.XPATH, using = "//div[@class = 'GQ8Pzc']")
    public WebElement LoginFail;
	
}
